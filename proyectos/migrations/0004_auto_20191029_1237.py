# Generated by Django 2.2.6 on 2019-10-29 11:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proyectos', '0003_remove_proyecto_id_proyecto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proyecto',
            name='fecha',
            field=models.DateField(auto_now_add=True),
        ),
    ]
