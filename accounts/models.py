from django.db import models
from django.contrib.auth.models import AbstractUser
from proyectos.models import Proyecto

# Create your models here.
class User(AbstractUser):
    cedula = models.CharField(max_length=200)
    telefono = models.CharField(max_length=200)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Users'

    def __str__(self):
        return self.username

# user: rivaldi password: Rivaldi123Falcon@